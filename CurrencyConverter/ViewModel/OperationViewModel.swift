//
//  OperationViewModel.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/17/22.
//

import Foundation

// MARK: - Operation ViewModel

/// The ViewModel for using each operation exchange
struct OperationViewModel: ViewModel {
    
    static let commissionPercent = CommissionCondition.commissionPercent
    
    var id = UUID()
    
    var source: CurrencyCode = .EUR
    var destination: CurrencyCode = .USD
    
    var sourceAmount: Decimal = .zero 
    var destinationAmount: Decimal = .zero
    var commissionFee: Decimal = .zero
}
