//
//  CurrencyExchangeViewModel.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/15/22.
//

import UIKit


// MARK: - Exchange Cell ViewModel

/// ViewModel for cells in Currency Exchange section
struct ExchangeCellViewModel {
    
    enum ExchangeType: String {
        case sell = "Sell"
        case receive = "Receive"
    }
    
    let type: ExchangeType
    let currency: CurrencyCode
    var icon: String {
        type == .sell ? "arrow.up" : "arrow.down"
    }
    var iconColor: UIColor {
        type == .sell ? UIColor.red : .green
    }
}

