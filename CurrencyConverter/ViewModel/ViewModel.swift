//
//  ViewModel.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/14/22.
//

import Foundation

// MARK: - Base ViewModel

/// We use this protocol to inherit the objects that need stored or used in the future growth of our app
protocol ViewModel: Identifiable, Codable, Hashable {
    var id: UUID { get set }
}

