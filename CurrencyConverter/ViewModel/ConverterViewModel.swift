//
//  ConverterViewModel.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/14/22.
//

import Foundation

// MARK: - Converter ViewModel

///App's primary view model.
///Handles most of the work plus requests to server and server response,
/// conversion, submit conversion, and others.
@MainActor
class ConverterViewModel: ObservableObject {
    
    @Published var operation = OperationViewModel()
    
    @Published var balances: [CurrencyCode: Decimal] =
        CurrencyCode.allCases.reduce(into: [CurrencyCode: Decimal]())
        { return $0[$1] = $1.initAmount }
    
    @Published var totalCommissionFee: [CurrencyCode: Decimal] =
        CurrencyCode.allCases.reduce(into: [CurrencyCode: Decimal]())
        { $0[$1] = Decimal.zero }
    
    var operationCounts: Int = 0

    weak var delegate: ConverterDelegate?

}

extension ConverterViewModel {
    /// Request conversion from the server
    func requestConversion(_ completion: @escaping (_ success: Bool, _ error: ConverterError?) -> Void) {
        let url = URL(string: "http://api.evp.lt/currency/commercial/exchange/\(operation.sourceAmount)-\(operation.source.rawValue)/\(operation.destination.rawValue)/latest")!
        Task {
            do {
                let (data, _) = try await URLSession.shared.data(from: url)
                if let decoded = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                   let json = decoded as? Dictionary<String, Any>,
                   let evm = try? CurrencyExchangeModel(dictionary: json) {
                       completion(true, nil)
                    operation.destinationAmount = evm.amount
                } else {
                    completion(false, ConverterError.failedConverting("Failed to decode server response data."))
                }
            } catch {
                completion(false, .failedNetwork("Failed: \(error.localizedDescription)"))
            }
        }
    }
    
    /// Starting conversion operation
    func submitConversion() {
        guard operation.sourceAmount > 0 &&
        operation.destinationAmount > 0 else { return }
        
        checkFreeCommission()
        
        let totalAmount = operation.sourceAmount + operation.commissionFee
        // If our balance will go negative state after the operation
        if !checkBalancesForDeduction() {
            let deficit = totalAmount - balances[operation.source]!
            delegate?.errorOccurred(.notEnoughAmount("Your balance has deficit!\n"
                + "\(deficit) \(operation.source) more you need."))
            return
        }
           
        // Request conversion from the web service
        requestConversion() { [weak self] success, error in
            
            guard let self = self else { return }
            
            if success {
                self.applyConversion(totalAmount)
            } else if let err = error {
                self.delegate?.errorOccurred(err)
            }
        }
    }
    
    /// Apply completed conversion operation.
    private func applyConversion(_ totalAmount: Decimal) {
        balances[operation.source]! -= totalAmount
        balances[operation.destination]! += operation.destinationAmount
        totalCommissionFee[operation.source]! += operation.commissionFee
        operationCounts += 1
        

        let src = operation.source
        let amount = "\(operation.sourceAmount) \(src)"
        let dstAmount = "\(operation.destinationAmount) \(operation.destination)"
        let fee = "\(operation.commissionFee) \(src)"
        let message = "You have converted \(amount) to \(dstAmount). Commission Fee - \(fee)"
        
        clearOperation()
        delegate?.currencyConverted(message)
    }
    
    func clearOperation() {
        operation.commissionFee = .zero
        operation.sourceAmount = .zero
        operation.destinationAmount = .zero
    }
    
    /// Check if it has any commission for the current conversion.
    func checkFreeCommission() {        
        if !CommissionCondition.checkConditions(operationCounts) {
            let fee = operation.sourceAmount * Decimal(OperationViewModel.commissionPercent)
            
            operation.commissionFee = fee.twoDigitFormatted()
        }
    }
    
    /// Check that balance never gets negative after conversion
    private func checkBalancesForDeduction() -> Bool {
        let amount = operation.sourceAmount + operation.commissionFee
        
        return balances[operation.source]! > amount
    }
}

/// Delegate that communicates with ViewControllers.
protocol ConverterDelegate: AnyObject {
    func errorOccurred(_ error: ConverterError)
    func currencyConverted(_ message: String)
}
