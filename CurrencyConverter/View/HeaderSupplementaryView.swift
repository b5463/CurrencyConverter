//
//  HeaderSupplementaryView.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import UIKit

class HeaderSupplementaryView: UICollectionReusableView {
    static let reusableIdentifier = "header-supplementary-reuse-identifier"
    static let elementKind = "section-header-element-kind"
    let title = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension HeaderSupplementaryView {
    private func configure() {
        title.translatesAutoresizingMaskIntoConstraints = false
        title.adjustsFontForContentSizeCategory = true
        title.font = UIFont.preferredFont(forTextStyle: .callout)
        title.textColor = .secondaryLabel
        addSubview(title)
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: ViewSize.inset * 3),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewSize.inset),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ViewSize.inset),
            title.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -ViewSize.inset)
        ])
        
    }
}
