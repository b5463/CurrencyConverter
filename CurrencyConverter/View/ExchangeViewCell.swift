//
//  ExchangeViewCell.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import UIKit

class ExchangeViewCell: UICollectionViewCell {
    
    weak var view: UIView?
    weak var delegate: ExchangeCurrencyDelegate?
    
    static let id: String = "exchangeCell"
    
    var exchange: ExchangeCellViewModel! {
        didSet {
            guard exchange != nil else { return }
            setupContent()
        }
    }
    
    private var statusIcon: UIImageView! = nil
    private var label: UILabel! = nil
    private var currencyButton: UIButton! = nil
    private var currentCurrency: CurrencyCode = .EUR
    private var amountLabel: UILabel! = nil
    private var amountInput: UITextField! = nil
    private var separator: UIView! = nil

    private var amount: Decimal! = nil
    private var tapGesture: UITapGestureRecognizer! = nil
    private var timer: Timer! = nil
    private var isChange: Bool = false
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        amount = .zero
        
        amountInput?.text = nil
        amountInput?.removeFromSuperview()
        amountInput = nil
        
        amountLabel?.text = nil
        amountLabel?.removeFromSuperview()
        amountLabel = nil
    }
    
}

// MARK: - Configure Hierarchy
extension ExchangeViewCell {
    private func configureCell() {
        configureIcon()
        configureLabel()
        configureCurrencyButton()
        configureSeparator()
        startTimer()
    }
    
    private func configureIcon() {
        
        let size: CGFloat = 40.0
        statusIcon = UIImageView()
        statusIcon.translatesAutoresizingMaskIntoConstraints = false
        statusIcon.contentMode = .center
        statusIcon.tintColor = .white
        statusIcon.layer.cornerRadius = size / 2
        
        contentView.addSubview(statusIcon)
        NSLayoutConstraint.activate([
            statusIcon.widthAnchor.constraint(equalToConstant: size),
            statusIcon.heightAnchor.constraint(equalToConstant: size),
            statusIcon.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            statusIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    private func configureLabel() {
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: statusIcon.trailingAnchor, constant: ViewSize.padding2),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    private func configureCurrencyButton() {
        currencyButton = UIButton()
        currencyButton.translatesAutoresizingMaskIntoConstraints = false
        currencyButton.menu = createUIMenu()
        currencyButton.setTitleColor(.black, for: .normal)
        currencyButton.showsMenuAsPrimaryAction = true
         
        contentView.addSubview(currencyButton)
        NSLayoutConstraint.activate([
            currencyButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            currencyButton.topAnchor.constraint(equalTo: contentView.topAnchor),
            currencyButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            currencyButton.widthAnchor.constraint(equalToConstant: 80)
        ])
    }
    
    private func configureAmountInput() {
        guard amountInput == nil else { return }
        
        amountInput = UITextField()
        amountInput.translatesAutoresizingMaskIntoConstraints = false
        amountInput.keyboardType = .decimalPad
        amountInput.text = "0.0"
        amountInput.textAlignment = .right
        amountInput.delegate = self
        
        contentView.addSubview(amountInput)
        NSLayoutConstraint.activate([
            amountInput.topAnchor.constraint(equalTo: contentView.topAnchor),
            amountInput.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: ViewSize.padding2),
            amountInput.trailingAnchor.constraint(equalTo: currencyButton.leadingAnchor, constant: -ViewSize.padding2),
            amountInput.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            amountInput.widthAnchor.constraint(greaterThanOrEqualTo: label.widthAnchor, multiplier: 1.5)
        ])
    }
    
    private func configureAmountLabel() {
        guard amountLabel == nil else { return }
        amountLabel = UILabel()
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        amountLabel.text = "0.0"
        amountLabel.textAlignment = .right
        
        contentView.addSubview(amountLabel)
        NSLayoutConstraint.activate([
            amountLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            amountLabel.leadingAnchor.constraint(equalTo: label.trailingAnchor,constant: ViewSize.padding2),
            amountLabel.trailingAnchor.constraint(equalTo: currencyButton.leadingAnchor, constant: -ViewSize.padding2),
            amountLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            amountLabel.widthAnchor.constraint(greaterThanOrEqualTo: label.widthAnchor, multiplier: 1.5)
        ])
    }
    
    private func configureSeparator() {
        separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = .systemGray4
        contentView.addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
    private func createUIMenu() -> UIMenu {
        
        var children: [UIMenuElement] = []
        for type in CurrencyCode.allCases {
            let image = type == currentCurrency ? UIImage(systemName: "checkmark") : nil
            children.append(UIAction(title: type.rawValue, image: image, discoverabilityTitle: type.rawValue) { [unowned self] action in
                currentCurrency = type
                setCurrencyButtonTitle()
                isChange = true
            })
        }
        
        return UIMenu(children: children)
    }
}

// MARK: - Content Changes
extension ExchangeViewCell {
    private func setupContent() {
        statusIcon.image = UIImage(systemName: exchange.icon, withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .bold))
        statusIcon.backgroundColor = exchange.iconColor
        label.text = exchange.type.rawValue
        currentCurrency = exchange.currency
        setCurrencyButtonTitle()
        
        if exchange.type == .sell {
            configureAmountInput()
        } else {
            configureAmountLabel()
        }
    }
    
    private func setCurrencyButtonTitle() {
        let attachment = NSTextAttachment(image: UIImage(systemName: "chevron.down")!)
        let str = NSAttributedString(attachment: attachment)
        let attr = NSMutableAttributedString(string: "\(currentCurrency.rawValue) ")
        attr.append(str)
        
        currencyButton.setAttributedTitle(attr, for: .normal)
        currencyButton.menu = createUIMenu()
    }
    
    /// Set timer for managing amount of requests
    private func startTimer() {
        timer = timer ?? Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerElapsed), userInfo: nil, repeats: true)
    }
    @objc
    private func timerElapsed() {
        guard isChange else { return }
        isChange = false
        
        delegate?.currencyTypeDidChange(currentCurrency, exchange.type)
        if exchange.type == .sell && amount ?? 0 > 0 {
            delegate?.amountDidChange(amount)
        }
    }
    
    func setAmount(_ amount: Decimal) {
        guard amountLabel != nil else { return }
        
        amountLabel.text = "\(amount > 0 ? "+ " : "0.")\(amount)"
        amountLabel.textColor = amount > 0 ? .green : .black
    }
}

extension ExchangeViewCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let txt = textField.text,
           txt.count > 0 {
            DispatchQueue.main.async {
                textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
            }
        }
        if tapGesture == nil {
            tapGesture = UITapGestureRecognizer()
            tapGesture.addTarget(self, action: #selector(unfocusTextField))
        }
        view?.addGestureRecognizer(tapGesture)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if tapGesture != nil {
            view?.removeGestureRecognizer(tapGesture)
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let text = textField.text,
           let decimal = Decimal(string: text) {
            amount = decimal
            isChange = true
        }
    }
    @objc
    private func unfocusTextField() {
        amountInput.endEditing(true)
    }
}

protocol ExchangeCurrencyDelegate: AnyObject {
    func amountDidChange(_ amount: Decimal)
    func currencyTypeDidChange(_ currency: CurrencyCode, _ type: ExchangeCellViewModel.ExchangeType)
}
