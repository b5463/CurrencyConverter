//
//  MainViewController.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import UIKit
import Combine

class MainViewController: UIViewController {
    
    enum SectionKind: Int, CaseIterable {
        case balance, exchange, commission
        
        var title: String {
            switch self {
            case .balance: return "My Balasces"
            case .exchange: return "Currency Exchange"
            case .commission: return "Commission Fees"
            }
        }
        var getItemIdentifiers: [Int] {
            let count = CurrencyCode.allCases.count
            switch self {
            case .balance: return Array(0..<count)
            case .exchange:
                let start = count
                let end = count + 1
                return Array(start...end)
            case .commission:
                let start = count + 2
                let end = count * 2 + 1
                return Array(start...end)
            }
        }
    }
    
    // MARK: - Properties
    private var collectionView: UICollectionView! = nil
    private var dataSource: UICollectionViewDiffableDataSource<SectionKind, Int>! = nil
    private var submitButton: UIButton! = nil
    private var submitBackground: UIVisualEffectView! = nil
    
    var converter: ConverterViewModel! = nil
    
    private var submitBottomConstraint: NSLayoutConstraint!
    private var cancellables: [AnyCancellable] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate()

        configure()
    }
}


// MARK: - Configure Hierarchy
extension MainViewController {
    private func configure() {
        initOperations()
        configureNavigation()
        configureNotifications()
        configureCollectionView()
        configureDataSource()
        configureSubmitButton()
        
        converter.delegate = self
        cancellables.append(contentsOf: [
                converter.$balances
                    .receive(on: RunLoop.main)
                    .removeDuplicates()
                    .sink { [unowned self] _ in
                        reloadSection(.balance)
                    },
                converter.$totalCommissionFee
                    .receive(on: RunLoop.main)
                    .removeDuplicates()
                    .sink { [unowned self] _ in
                        reloadSection(.commission)
                    }
            ])
    }
    
    private func initOperations() {
        converter = ConverterViewModel()
        converter.delegate = self
    }
    
    private func configureNotifications() {
        
        let notifications = [UIResponder.keyboardWillChangeFrameNotification,
                                     UIResponder.keyboardWillHideNotification]
        let nc = NotificationCenter.default
        
        cancellables.append(Publishers.Merge(
                nc.publisher(for: notifications[0]),
                nc.publisher(for: notifications[1])
            )
            .sink(receiveValue: { [unowned self] notification in
                adjustForKeyboardNotification(notification)
            })
        )
    }
    
    private func configureCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        view.addSubview(collectionView)
    }
    
    private func configureSubmitButton() {
        submitButton = SubmitButton(colors: [UIColor.customBlue1.cgColor, UIColor.customBlue2.cgColor])
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.setTitle("SUBMIT", for: .normal)
        submitButton.setTitleColor(.white, for: .normal)
        submitButton.addTarget(self, action: #selector(submitClicked), for: .touchUpInside)
        
        view.addSubview(submitButton)
        submitBottomConstraint = submitButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -25)
        let inset: CGFloat = 35
        NSLayoutConstraint.activate([
            submitButton.heightAnchor.constraint(equalToConstant: 50),
            submitButton.widthAnchor.constraint(equalTo: view.layoutMarginsGuide.widthAnchor, constant: -inset),
            submitButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            submitBottomConstraint
        ])
        
        configureSubmitBackground()
    }
    
    private func configureSubmitBackground() {
        submitBackground = UIVisualEffectView()
        submitBackground.effect = UIBlurEffect(style: .light)
        submitBackground.translatesAutoresizingMaskIntoConstraints = false
                
        view.insertSubview(submitBackground, belowSubview: submitButton)
        NSLayoutConstraint.activate([
            submitBackground.centerXAnchor.constraint(equalTo: submitButton.centerXAnchor),
            submitBackground.centerYAnchor.constraint(equalTo: submitButton.centerYAnchor),
            submitBackground.widthAnchor.constraint(equalTo: view.widthAnchor),
            submitBackground.heightAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 2)
        ])
    }
                               
    @objc
    private func submitClicked() {
        converter.submitConversion()
    }
}

// MARK: - UICollectionView DataSource and Layout
extension MainViewController {
    
    private func configureDataSource() {
        
        let currencyCellRegistration = UICollectionView.CellRegistration<CurrencyViewCell, Int> { [unowned self] cell, indexPath, id in
            
            let currency = CurrencyCode.allCases[indexPath.row]
            let amount = indexPath.section == 0 ?
                converter.balances[currency]! :
                converter.totalCommissionFee[currency]!
            cell.setText(amount, currency)
        }
        
        let exchangeCellRegistration = UICollectionView.CellRegistration<ExchangeViewCell, ExchangeCellViewModel.ExchangeType> { [unowned self] cell, indexPath, type in
            let currency = type == .sell ? converter.operation.source : converter.operation.destination
            cell.exchange = ExchangeCellViewModel(type: type, currency: currency)
            
            if type == .sell {
                cell.view = view
            } else {
                cell.setAmount(converter.operation.destinationAmount)
            }
            cell.delegate = self
        }
        
        let headerRegistration = UICollectionView.SupplementaryRegistration<HeaderSupplementaryView>(elementKind: HeaderSupplementaryView.elementKind) { supplementaryView, elementKind, indexPath in
            let section = SectionKind(rawValue: indexPath.section)!
            supplementaryView.title.text = section.title.uppercased()
        }
        
        dataSource = UICollectionViewDiffableDataSource<SectionKind, Int>(collectionView: collectionView) { collectionView, indexPath, row -> UICollectionViewCell? in
            
            if indexPath.section == 1 {
                let type = indexPath.row == 0 ? ExchangeCellViewModel.ExchangeType.sell : .receive
                return collectionView.dequeueConfiguredReusableCell(using: exchangeCellRegistration, for: indexPath, item: type)
            } else {
                return collectionView.dequeueConfiguredReusableCell(using: currencyCellRegistration, for: indexPath, item: row)
            }
                
        }
        
        dataSource.supplementaryViewProvider = { [unowned self] view, kind, index in
            collectionView.dequeueConfiguredReusableSupplementary(using: headerRegistration, for: index)
        }
        
        var snapshot = NSDiffableDataSourceSnapshot<SectionKind, Int>()
        snapshot.appendSections(SectionKind.allCases)
        for section in SectionKind.allCases {
            snapshot.appendItems(section.getItemIdentifiers, toSection: section)
        }
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    private func reloadSection(_ section: SectionKind) {
        var snapshot = dataSource.snapshot()
        let items = snapshot.itemIdentifiers(inSection: section)
        snapshot.reconfigureItems(items)
        dataSource.apply(snapshot)
    }
    private func reloadSellCell() {
        var snapshot = dataSource.snapshot()
        if let item = snapshot.itemIdentifiers(inSection: .exchange).first {
            snapshot.reconfigureItems([item])
            dataSource.apply(snapshot)
        }
    }
    private func reloadReceiveCell() {
        var snapshot = dataSource.snapshot()
        if let item = snapshot.itemIdentifiers(inSection: .exchange).last {
            snapshot.reconfigureItems([item])
            dataSource.apply(snapshot)
        }
    }
    
    /// Create layout based on section kind
    private func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            
            guard let sectionKind = SectionKind(rawValue: sectionIndex) else { return nil }
            
            let itemSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            // If is the Currency Exchange section then use whole width
            //  for size else estimated size of 160
            let groupWidth = sectionKind == .exchange ?
            NSCollectionLayoutDimension.fractionalWidth(1.0) :
            NSCollectionLayoutDimension.estimated(160.0)
            
            let groupSize = NSCollectionLayoutSize(
                widthDimension: groupWidth, heightDimension: .absolute(60))
            
            let group = sectionKind == .exchange ?
            NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1) :
            NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
            
            let section = NSCollectionLayoutSection(group: group)
            if sectionKind != .exchange {
                section.orthogonalScrollingBehavior = .groupPaging
            }
            
            // Add section header
            let headerSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .estimated(44))
            let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: headerSize,
                elementKind: HeaderSupplementaryView.elementKind,
                alignment: .top
            )
            section.boundarySupplementaryItems = [sectionHeader]
            
            return section
        }
        return layout
    }
}

// MARK: - Keyboard notification
extension MainViewController {
    private func adjustForKeyboardNotification(_ notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            submitBottomConstraint.constant = -25
            collectionView.contentInset = .zero
        } else {
            let offset = keyboardViewEndFrame.height - view.safeAreaInsets.bottom
            
            collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: offset, right: 0)
            collectionView.scrollToItem(at: IndexPath(row: 0, section: 1), at: .centeredVertically, animated: true)
            submitBottomConstraint.constant = -(offset + 25)
        }
        view.layoutIfNeeded()
    }
}

// MARK: - ExchangeViewDelegate
extension MainViewController: ExchangeCurrencyDelegate {
    func amountDidChange(_ amount: Decimal) {
        // prevent from double request
        guard converter.operation.sourceAmount != amount else { return }
        
        converter.operation.sourceAmount = amount
        if amount > 0 {
            converter.operation.destinationAmount = 0
            converter.requestConversion() { [weak self] success, error in
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    if success {
                        self.reloadReceiveCell()
                    } else if let err = error {
                        self.alertMessage("Failed to convert", err)
                    }
                }
            }
        }
    }
    func currencyTypeDidChange(_ currency: CurrencyCode, _ type: ExchangeCellViewModel.ExchangeType) {
        var isChange = false
        if type == .sell {
            isChange = converter.operation.source != currency
            converter.operation.source = currency
        } else {
            isChange = converter.operation.destination != currency
            converter.operation.destination = currency
        }
        if isChange {
            converter.clearOperation()
            reloadSection(.exchange)
        }
    }
}

// MARK: - Converter Delegate
extension MainViewController: ConverterDelegate {
    func errorOccurred(_ error: ConverterError) {
        alertMessage("Could not convert", error)
    }
    
    func currencyConverted(_ message: String) {
        alertMessage("Currency converted", message, actionTitle: "Done")
        
        // reload Currency Exchange and reset their amounts after the operation has ended.
        reloadSection(.exchange)
    }
}

// MARK: - Navigation Appearance
extension MainViewController {
    private func configureNavigation() {
        navigationItem.title = "Corrency converter"
        guard let bounds = navigationController?.navigationBar.bounds else { return }
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundImage =
        UIImage.gradientImage(bounds: bounds,
                              colors: [UIColor.customBlue1.cgColor, UIColor.customBlue2.cgColor])
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
    }
}


