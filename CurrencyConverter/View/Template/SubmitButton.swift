//
//  SubmitButton.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/15/22.
//

import UIKit

class SubmitButton: UIButton {
    
    private var colors: [CGColor]
    init(colors: [CGColor]) {
        self.colors = colors
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
    private lazy var gradientLayer: CAGradientLayer = {
       let gl = CAGradientLayer()
        gl.frame = self.bounds
        gl.colors = colors
        gl.startPoint = CGPoint(x: 0, y: 0.5)
        gl.endPoint = CGPoint(x: 1, y: 0.5)
        gl.cornerRadius = self.bounds.height / 2
        layer.insertSublayer(gl, at: 0)
        return gl
    }()
    
}
