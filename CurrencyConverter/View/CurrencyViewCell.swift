//
//  CurrencyViewCell.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import UIKit

class CurrencyViewCell: UICollectionViewCell {
    
    private var amountLabel: UILabel! = nil
    private var currencyLabel: UILabel! = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CurrencyViewCell {
    private func configure() {
        configureAmountLabel()
        configureCurrencyLabel()
    }
    
    private func configureAmountLabel() {
        amountLabel = UILabel()
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        amountLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        contentView.addSubview(amountLabel)
        NSLayoutConstraint.activate([
            amountLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            amountLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    private func configureCurrencyLabel() {
        currencyLabel = UILabel()
        currencyLabel.translatesAutoresizingMaskIntoConstraints = false
        currencyLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        contentView.addSubview(currencyLabel)
        NSLayoutConstraint.activate([
            currencyLabel.leadingAnchor.constraint(equalTo: amountLabel.trailingAnchor, constant: ViewSize.padding),
            currencyLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    func setText(_ amount: Decimal, _ currency: CurrencyCode) {
        amountLabel.text = currency == .JPY ? amount.formatted() : amount.toString()
        currencyLabel.text = currency.rawValue
    }
}
