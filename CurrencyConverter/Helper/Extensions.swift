//
//  Extensions.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import UIKit

/// Create two functions for simplifying showing the user any alert
extension UIViewController {
    func alertMessage(_ title: String?, _ message: String, actionTitle: String = "OK", actionStyle: UIAlertAction.Style = .default) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: actionStyle)
        alert.addAction(action)
        
        present(alert, animated: true)
    }
    func alertMessage(_ title: String?, _ error: ConverterError) {
        var message = ""
        switch error {
        case .failedNetwork(let msg):
            message = msg
        case .notEnoughAmount(let msg):
            message = msg
        case .failedConverting(let msg):
            message = msg
        }
        
        alertMessage(title, message)
    }
}

extension Decimal {
    /// Showing Decimal value with formatted style and adding ".00" if the value was "0".
    func toString() -> String {
        var str = "\(self.formatted())"
        str = str.contains(".") ? str : str + ".00"
        return str
    }
    /// Get Decimal with a two-digit fraction formatted style.
    func twoDigitFormatted() -> Decimal {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        let numberString = formatter.string(from: self as NSNumber)!
        return Decimal(string: numberString)!
    }
}

extension UIImage {
    /// Create a gradient image with custom colors and directions.
    static func gradientImage(bounds: CGRect, colors: [CGColor], startPoint: CGPoint = CGPoint(x: .zero, y: 0.5), endPoint: CGPoint = CGPoint(x: 1.0, y: 0.5)) -> UIImage {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        gradient.colors = colors
        
        UIGraphicsBeginImageContext(gradient.bounds.size)
        gradient.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
