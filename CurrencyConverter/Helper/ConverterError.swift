//
//  ConverterError.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/16/22.
//

import Foundation

// MARK: - Custom Error 
enum ConverterError: Error {
    case failedNetwork(_ message: String)
    case failedConverting(_ message: String)
    case notEnoughAmount(_ message: String)
}
