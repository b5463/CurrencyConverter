//
//  CommissionCondition.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/17/22.
//

import Foundation

// MARK: - Commission Condition

/// We put all the conditions here for whether to charge the user or not
enum CommissionCondition: CaseIterable {
    case freeCounts
    
    // 0.7% == 0.007
    static let commissionPercent = 0.007
    
    private static let counts: Int = 5
    
    static func checkConditions(_ operationCounts: Int) -> Bool {
        for member in CommissionCondition.allCases {
            switch member {
            case .freeCounts:
                if counts <= operationCounts {
                    return false
                }
            }
        }
        return true
    }
}
