//
//  CustomColors.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/17/22.
//

import UIKit

// MARK: - Custom Colors

/// Add our custom color from Assets Color Set to use in our code.
extension UIColor {
    /// #0084CA dark blue
    static let customBlue1 = UIColor(named: "color_0084ca")!
    
    /// #009DE3 light blue
    static let customBlue2 = UIColor(named: "color_009de3")!
}
