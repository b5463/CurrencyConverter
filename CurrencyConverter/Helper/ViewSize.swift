//
//  ViewSize.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/15/22.
//

import UIKit

// MARK: - General Measure Usage
enum ViewSize {
    
    /// CGFloat 10
    static var inset: CGFloat { 10 }
    
    /// CGFloat 5
    static var padding: CGFloat { 5 }
    
    /// CGFloat 15
    static var padding2: CGFloat { 15 }
}
