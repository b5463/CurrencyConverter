//
//  CurrencyExchangeModel.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/17/22.
//

import Foundation

// MARK: - Server Response Model
struct CurrencyExchangeModel: Codable {
    let amount: Decimal
    let currency: CurrencyCode
    
    init(amount: Decimal, currency: CurrencyCode) {
        self.amount = amount
        self.currency = currency
    }
    
    /// Create our model directly from server response dictionary
    init(dictionary: [String: Any]) throws {
        guard let amountStr = dictionary["amount"] as? String,
              let amount = Decimal(string:  amountStr),
              let currencyStr = dictionary["currency"] as? String,
              let currency = CurrencyCode(rawValue: currencyStr) else {
            
            throw ConverterError.failedConverting("Failed convert data model")
        }
        self.amount = amount
        self.currency = currency
    }
}
