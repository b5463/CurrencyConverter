//
//  CurrencyCode.swift
//  CurrencyConverter
//
//  Created by Ali Rezaei on 4/17/22.
//

import Foundation

// MARK: - Currency Code

///  This is our currency supported code and we could add other code here
///   for supporting new currency
enum CurrencyCode: String, CaseIterable, Codable {
    case EUR = "EUR"
    case USD = "USD"
    case JPY = "JPY"
    
    var initAmount: Decimal {
        switch self {
        case .EUR: return 1000
        default: return .zero
        }
    }
}
